<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration 
{

        public function up() 
        {
                Schema::create('users', function($t) {
                        $t->increments('id');
                        $t->string('username', 16)->unique();
                        $t->string('password', 64);
                        $t->integer('role_id')->unsigned();
                        $t->timestamps();
                        $t->rememberToken();
                });
        }

        public function down() 
        {
                Schema::drop('users');
        }

}