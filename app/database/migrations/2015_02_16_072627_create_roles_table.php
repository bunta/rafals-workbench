<?php

use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration 
{

        public function up() 
        {
                Schema::create('roles', function($t) {
                        $t->increments('id');
                        $t->string('name', 40)->unique();
                        
                });
        }
        
        public function down() 
        {
                Schema::drop('roles');
        }
        
}