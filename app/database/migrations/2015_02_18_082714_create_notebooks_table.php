<?php

use Illuminate\Database\Migrations\Migration;

class CreateNotebooksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		        Schema::create('notebooks', function($t) {
                        $t->increments('id');
                        $t->string('model', 50);
                        $t->string('procesor', 30);
                        $t->string('graphic',50);
                        $t->integer('ram')->unsigned();
                        $t->integer('hard_drive')->unsigned();
                        $t->double('diagonal',3,1);
                        $t->string('system',40);
                        $t->double('height',3,1);
                        $t->double('width',3,1);
                        $t->double('thickness',2,1);
                        $t->double('weight',2,1);

                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notebooks');
	}

}
