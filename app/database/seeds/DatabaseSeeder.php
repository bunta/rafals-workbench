<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

	 	$this->call('RolesTableSeeder');
	 	$this->command->info('Roles table seeded!');
	 	$this->call('UsersTableSeeder');
	 	$this->command->info('Users table seeded!');	
	 	$this->call('NotebooksTableSeeder');
	    $this->command->info('Notebooks table seeded!');


	}

}
