<?php

class UsersTableSeeder extends Seeder {

    public function run()
    {

        $admin_role = DB::table('roles')
                                ->select('id')
                                ->where('name', 'admin')
                                ->first()
                                ->id;
        $user_role = DB::table('roles')
                                ->select('id')
                                ->where('name', 'user')
                                ->first()
                                ->id;
        $mod_role  = DB::table('roles')
                                ->select('id')
                                ->where('name', 'moderator')
                                ->first()
                                ->id;
        DB::table('users')->delete();
        User::create(array(
                'username' => 'admin',
                'password' => Hash::make('asd'),
                'role_id'  => $admin_role,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
        User::create(array(
                'username' => 'mod',
                'password' => Hash::make('asd'),
                'role_id'  => $mod_role,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
        User::create(array(
                'username' => 'user',
                'password' => Hash::make('asd'),
                'role_id'  => $user_role,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));    
    }
}