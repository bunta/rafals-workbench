<?php

class NotebooksTableSeeder extends Seeder {

    public function run()
    {
        DB::table('notebooks')->delete();
        Notebook::create(array(
                'model' 		=> 'GE70 Apache Pro',
                'procesor' 		=> 'Intel Core i7-4710HQ',
                'graphic' 		=> 'NVIDIA GeForce GTX 860M',
                'ram' 			=> '16',
                'hard_drive' 	=> '1000',
                'diagonal' 		=> '17.3',
                'system' 		=> 'No operating system',
                'height' 		=> '41.8',
                'width' 		=> '26.9',
                'thickness' 	=> '2.2',
                'weight' 		=> '3.1'
        ));
        
    }
}
