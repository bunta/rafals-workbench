<?php

class RolesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('roles')->delete();
        Roles::create(array(
                'name' => 'admin'
        ));
        Roles::create(array(
                'name' => 'user'
        ));
        Roles::create(array(
                'name' => 'moderator'
        ));
    }
}
