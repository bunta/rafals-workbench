<?php

class Notebook extends Eloquent {

		protected $table = 'notebooks';
		public $timestamps = false;
		protected $guarded = array();
		protected $fillable = ['model',
									'procesor',
									'graphic',
									'ram',
									'hard_drive',
									'diagonal',
									'system',
									'height',
									'width',
									'thickness',
									'weight'
		];

		public static $rules = [
			'model' => 'required|max:50',
            'procesor' => 'required|max:30',
            'graphic' => 'required|max:50',
            'ram' => 'required|integer',
            'hard_drive' => 'required|integer',
            'diagonal' => array('required', 'regex:/^\d{2}\.\d{1}$/'), // np 33.3
            'system' => 'required|max:50',
            'height' => array('required', 'regex:/^\d{2}\.\d{1}$/'), // np 33.3
            'width' => array('required', 'regex:/^\d{2}\.\d{1}$/'), // np 33.3
            'thickness' => array('required', 'regex:/^\d{1}\.\d{1}$/'), // np 3.3
            'weight' => array('required', 'regex:/^\d{1}\.\d{1}$/') // np 3.3
        ];

}