<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Route::model();
//Notebook model
Route::model('notebook', 'Notebook');

Route::get('/', function()
{
    return View::make('home');
});

Route::get('/login','LoginController@show');
Route::post('/login','LoginController@login');
Route::get('/logout','LoginController@logout');

Route::get('/registration','RegistrationController@show');
Route::post('/registration','RegistrationController@store');

Route::get('/dashboard', array('before' => 'auth', 'uses' => 'DashboardController@show'));

Route::get('/editabledashboard',array('before' => 'auth', 'uses' => 'EditableDashboardController@show'));
Route::post('store',array('uses' => 'EditableDashboardController@store'));
Route::post('delete',array('uses' => 'EditableDashboardController@delete'));
Route::post('edit',array('uses' => 'EditableDashboardController@edit'));

//Ajax
Route::get('/ajax/notebook/show/{notebook?}', 'Ajax_NotebookController@show');
Route::post('submit','Ajax_NotebookController@edit');
/*Route::post('submit',function(){
	$validator = Validator::make(
		array(
			'model' => Input::get('model')
			),
		array(
			'model' => 'required|max:50'
			)
		);
		if($validator->fails()){
			return Response::json([
				'success' => false,
				'errors'  => $validator->errors()->toArray()
				]);
		}
		return Response::json(['success' => true ]);
});*/