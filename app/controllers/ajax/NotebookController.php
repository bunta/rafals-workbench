<?php


class Ajax_NotebookController extends \BaseController 
{

	/**
	 * Pobieramy obiekt 
	 * @return JSON tablica json z obiektem
	 */
	public function show($id)
	{
		return Response::json($id);
	}


	/**
	 *Usuwamy instancje obiektu z tabeli
	 *@return boolean true
	 */
	public function destroy($id)
	{
		$id->delete();
		return Response::json(true);
	}

	/**
	 *Kontrola edytowanych pol
	 *@return
	 */

	public function edit(){

		$validator = Validator::make(Input::all(),Notebook::$rules);

		if($validator->fails()){
			return Response::json([
				'success' => false,
				'errors'  => $validator->errors()->toArray()
				]);
		}else{
			Response::json(['success' => true ]);
		}
	}
	
}