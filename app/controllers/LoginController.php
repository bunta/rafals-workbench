<?php



class LoginController extends BaseController {

    /**
     *  Wyświetlenie widoku logowania
     */
    public function show() {
        return View::make('login');
    }
    
    /**
     *
     */
    public function login() {
        if( Auth::check()){
            return Redirect::action('DashboardController@show');
        }else{

            if( Input::method() == 'POST' ){
                $user = array(
                    'username' => Input::get('username'),
                    'password' => Input::get('password')
                );

                if( Auth::attempt($user)){
                    if( Auth::user()->role_id == 2 ){
                        return Redirect::action('DashboardController@show');
                    }else{
                        return Redirect::action('EditableDashboardController@show');
                    }
                }else{
                    Log::info('logowanie sie nie powiodlo');
                    return View::make('login');
                }

                
            }
        }

    }

    public function logout(){
        if(Auth::check()) {
            Auth::logout();
            return Redirect::to('/');
        }else {
            return Redirect::to('/login');
        }
    }

}