<?php

    /**
     * Kontroler odpowiadajacy za czesc administracyjna dashboardu 
     *@return boolean true
     */

class EditableDashboardController extends BaseController {

    public function show() {
        return View::make('editabledashboard')->withNotebooks(Notebook::get());

    }

    public function edit() {

        $validator = Validator::make(Input::all(),Notebook::$rules);
        
        if( $validator->fails()){
            return Redirect::action('EditableDashboardController@show')->withErrors($validator);
        
        }else{

            $notebook = Notebook::find(Input::get('notebook_id'));
            $notebook = new Notebook(Input::all());

            $notebook->save();
            return Redirect::action('EditableDashboardController@show');
        }
    }

    public function delete(){
        $notebook_id = Input::get('notebook_id');
        Notebook::destroy($notebook_id);
        return Redirect::action('EditableDashboardController@show');
    }



    public function store() {

        $validator = Validator::make(Input::all(),Notebook::$rules);

        if( $validator->fails()){
            return Redirect::action('EditableDashboardController@show')->withErrors($validator);
        }else{
        	$notebook = new Notebook(Input::all());
        	if( $notebook->save() ){
        		return Redirect::action('EditableDashboardController@show');
        	}
        } 

    }

}