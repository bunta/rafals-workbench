<?php


class RegistrationController extends BaseController {

    public function show() {
        return View::make('registration');
    }

    public function create() {
        //      
    }

    public function store() {

      $validator = Validator::make(Input::all(),User::$rules);

      if( $validator->fails()){
        return Redirect::action('RegistrationController@show')->withErrors($validator);
      }else{
        $user = new User;
        $user->username = Input::get('username');
        $user->password = Hash::make(Input::get('password'));
        $user->role_id = 2;
        $user->save();
        return View::make('receive')->with('username', $user->username);
      }
    }
}