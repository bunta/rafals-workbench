@extends('layouts/default')

@section('content')
	<p>This is receive view content.</p>

	<h2>We are back from controller!</h2>

      @if ($errors->has())
          <div class="alert alert-danger">
              @foreach ($errors->all() as $error)
                  {{ $error }}<br>        
              @endforeach
          </div>
      @endif


    <table class="table table-hover">
      <thead>
        <tr>
          <th>Model</th>
          <th>Procesor</th>
          <th>Graphic</th>
          <th>Ram</th>
          <th>Hard Drive</th>
          <th>Diagonal</th>
          <th>System</th>
          <th>Height</th>
          <th>Width</th>
          <th>Thickness</th>
          <th>Weight</th>
          <th>Remove</th>
          <th>Edit</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($notebooks as $n)
        <!--<a href="#">-->
       		<tr>
       			<td>{{$n->model}}</td>
       			<td>{{$n->procesor}}</td>
       			<td>{{$n->graphic}}</td>
       			<td>{{$n->ram}}</td>
       			<td>{{$n->hard_drive}}</td>
       			<td>{{$n->diagonal}}</td>
       			<td>{{$n->system}}</td>
       			<td>{{$n->height}}</td>
       			<td>{{$n->width}}</td>
       			<td>{{$n->thickness}}</td>
       			<td>{{$n->weight}}</td>
            <td>  
              {{ Form::open(array('url' => 'delete'))}}
                {{ Form::hidden('notebook_id', $n->id) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-info'))}}
              {{ Form::close()}}
            </td>
            <td>
              <button type="button" id="notebook_{{$n->id}}" class="btn btn-info modaleEditBtn">
                Edit notebook
              </button>
            </td>
        	</tr>
    	<!--</a>-->
        @endforeach
      </tbody>
    </table>

  <script>
  
    $(document).ready(function(){
      var info = $("#er");
      $(".modaleEditBtn").click(function(){
          $("#myModal2").modal("show");
          //Akcja ajaxowa do pobrania
          info.hide().empty();
          var $id = this.id.split("notebook_")[1];
          $.get('{{url("/")}}/ajax/notebook/show/'+$id,function(callback){
              $.each(callback, function(k, v){
                $("#" + k ).val(v);           
              });
          });
      });
    });

    $(document).ready(function(){
      var info = $("#er");
      $('#myModal,#myModal2').submit(function(e){
        e.preventDefault();
        var formData = new FormData();
        formData.append('model',$('#model').val());
        var formData = $("#myModa2,#myModal").map(function(){
          return $(this).serializeArray();
        });
        $.ajax({
          url: 'submit',
          method: 'post',
          processData: false,
          contentType: false,
          cache:false,
          dataType: 'json',
          data: formData,
          success: function(data){
            info.hide().empty();
            if(!data.success){
              $.each(data.errors, function(index,error){
                info.text(error);
              });
              info.slideDown();
            }
          },
          error:function(){}
        });
      });
    });
  </script>



<!-- Button trigger modal -->
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">
  Add Notebook
</button>


<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit notebook</h4>
        <div id="er" class="alert alert-danger" style="display:none;">

        </div>
      </div>
      <div class="modal-body">
        {{ Form::open(array('url' => 'edit'))}}
          {{ Form::hidden('notebook_id', null,['id' => 'id']) }}
          <br />
          {{ Form::label('model','model')}}
          {{ Form::text('model',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('procesor','procesor')}}
          {{ Form::text('procesor',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('graphic','graphic')}}
          {{ Form::text('graphic',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('ram','ram(gb)')}}
          {{ Form::text('ram',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('hard_drive','hard_drive(gb)')}}
          {{ Form::text('hard_drive',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('diagonal','diagonal(inch)')}}
          {{ Form::text('diagonal',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('system','system')}}
          {{ Form::text('system',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('height','height(cm)')}}
          {{ Form::text('height',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('width','width(cm)')}}
          {{ Form::text('width',null,['class' => 'form-control'])}}
          <br />       
          {{ Form::label('thickness','thickness(cm)')}}
          {{ Form::text('thickness',null,['class' => 'form-control'])}}
          <bn->
          {{ Form::label('weight','weight(kg)')}}
          {{ Form::text('weight',null,['class' => 'form-control'])}}
          <br />
          {{ Form::submit('Save notebook', array('class' => 'btn btn-info'))}}
        {{ Form::close()}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

    <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add notebook</h4>
        <div class="alert alert-danger" style="display:none;">
          <ul></ul>
        </div>
      </div>
      <div class="modal-body">
        {{ Form::open(array('url' => 'store'))}}
          {{ Form::label('model','model')}}
          {{ Form::text('model',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('procesor','procesor')}}
          {{ Form::text('procesor',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('graphic','graphic')}}
          {{ Form::text('graphic',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('ram','ram(gb)')}}
          {{ Form::text('ram',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('hard_drive','hard_drive(gb)')}}
          {{ Form::text('hard_drive',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('diagonal','diagonal(inch)')}}
          {{ Form::text('diagonal',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('system','system')}}
          {{ Form::text('system',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('height','height(cm)')}}
          {{ Form::text('height',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('width','width(cm)')}}
          {{ Form::text('width',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('thickness','thickness(cm)')}}
          {{ Form::text('thickness',null,['class' => 'form-control'])}}
          <br />
          {{ Form::label('weight','weight(kg)')}}
          {{ Form::text('weight',null,['class' => 'form-control'])}}
          <br />
          {{ Form::submit('Add Item', array('class' => 'btn btn-info'))}}
        {{ Form::close()}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@stop