@extends('layouts/default')

@section('content')
	<p>This is registration view content.</p>

	<h2>Register!</h2>
		@if ($errors->has())
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                {{ $error }}<br>        
	            @endforeach
	        </div>
	    @endif  
	<div class="textboxsize">
		{{ Form::open(array('action' => 'RegistrationController@store'))}}
			{{ Form::label('username','username')}}
			{{ Form::text('username',null,['class' => 'form-control'])}}
			<br />
			{{ Form::label('password','password')}}
			{{ Form::password('password', array('placeholder'=>'Password', 'class'=>'form-control' ) ) }}
			<br />
			{{ Form::submit('Register', array('class' => 'btn btn-info'))}}
		{{ Form::close()}}
	</div>
	
@stop

