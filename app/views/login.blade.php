@extends('layouts/default')

@section('content')
	<p>This is login view content.</p>

	<h2>Login!</h2>
	<div class="textboxsize">
	{{ Form::open(array('action' => 'LoginController@login'))}}
		{{ Form::label('username','username')}}
		{{ Form::text('username',null,['class' => 'form-control'])}}
		<br />
		{{ Form::label('password','password')}}
		{{ Form::password('password', array('placeholder'=>'Password', 'class'=>'form-control' ) ) }}
		<br />
		{{ Form::submit('Login', array('class' => 'btn btn-info'))}}
	{{ Form::close()}}

	</div>

@stop

