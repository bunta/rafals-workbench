@extends('layouts/default')

@section('content')
	<p>This is receive view content.</p>

	<h2>We are back from controller!</h2>


    <table class="table table-hover">
      <thead>
        <tr>
          <th>Model</th>
          <th>Procesor</th>
          <th>Graphic</th>
          <th>Ram</th>
          <th>Hard Drive</th>
          <th>Diagonal</th>
          <th>System</th>
          <th>Height</th>
          <th>Width</th>
          <th>Thickness</th>
          <th>Weight</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($notebooks as $n)
        <!--<a href="#">-->
       		<tr>
       			<td>{{$n->model}}</td>
       			<td>{{$n->procesor}}</td>
       			<td>{{$n->graphic}}</td>
       			<td>{{$n->ram}}</td>
       			<td>{{$n->hard_drive}}</td>
       			<td>{{$n->diagonal}}</td>
       			<td>{{$n->system}}</td>
       			<td>{{$n->height}}</td>
       			<td>{{$n->width}}</td>
       			<td>{{$n->thickness}}</td>
       			<td>{{$n->weight}}</td>
        	</tr>
    	<!--</a>-->
        @endforeach
      </tbody>
    </table>


@stop