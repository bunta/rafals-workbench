<!doctype html>
<html lang="pl">
    <head>
      <meta charset="UTF-8">
      <title></title>
      {{ HTML::style('css/bootstrap.min.css'); }}
      {{ HTML::style('css/bootstrap-theme.min.css'); }}
      {{ HTML::style('css/simplecss.css'); }}
      {{ HTML::script('js/jquery-2.1.3.min.js'); }}
      {{ HTML::script('js/bootstrap.min.js'); }}
      <style>
        .textboxsize{
            width: 20em;
        } 
        .bannersize{
            width: 500px;
        }
      </style>
      @yield('head')
    </head>
    <body>
      <header>
        <nav class="navbar navbar-default">
          <div class="container-fluid bgcolor">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{URL::to('/')}}">
                <img class="navbarlogo" alt="Brand" src="{{ URL::to('img/brand.gif') }}">
              </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav nav-justified"><!--navbar-nav">-->
                <li><a class="btn btn-default navbar-btn btn-lg" href="{{URL::to('/')}}" role="button">Home</a></li>
                <li><a class="btn btn-default navbar-btn btn-lg" href="{{URL::to('registration')}}" role="button">Registration</a></li>
                @if( Auth::check() )
                  <li><a class="btn btn-default navbar-btn btn-lg" href="{{URL::to('logout')}}" role="button">Logout</a></li>
                  @if( Auth::user()->role_id == 2 )
                    <li><a class="btn btn-default navbar-btn btn-lg" href="{{URL::to('dashboard')}}" role="button">Dashboard</a></li>    
                  @else
                    <li><a class="btn btn-default navbar-btn btn-lg" href="{{URL::to('editabledashboard')}}" role="button">Dashboard</a></li>
                  @endif
                @else
                  <li><a class="btn btn-default navbar-btn btn-lg" href="{{URL::to('login')}}" role="button">Login</a></li>
                @endif
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

      </header>

      <div class="container">
        @yield('content')
      </div>


    </body>
</html>
